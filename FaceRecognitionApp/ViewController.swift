
import UIKit
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    // MARK: - PROPERTIES
    var photoImageView = UIImageView()
    var photoImage = UIImage(named: "people")!
    // MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI() // отрисовываем элементы UI
    }
    // MARK: - CONFIGURE UI
    private func configureUI() {
        drawImage()
        drawButtons()
    }
    // отрисовка изображения
    private func drawImage() {
        photoImageView.image = photoImage
        photoImageView.contentMode = .scaleAspectFill
        let scaleHeight = view.frame.width / photoImage.size.width * photoImage.size.height
        photoImageView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: scaleHeight)
        view.addSubview(photoImageView)
    }
    // отрисовка кнопок
    private func drawButtons() {
        let changeImageButton = UIButton()
        changeImageButton.frame = CGRect(x: 0, y: photoImageView.frame.height + 200, width: 250, height: 50)
        changeImageButton.setTitle("Сменить изображение", for: .normal)
        changeImageButton.setTitleColor(.black, for: .normal)
        changeImageButton.layer.borderColor = UIColor.black.cgColor
        changeImageButton.layer.borderWidth = 1
        changeImageButton.center.x = view.center.x
        view.addSubview(changeImageButton)
        
        let nikolaevImageButton = UIButton()
        nikolaevImageButton.frame = CGRect(x: 0, y: changeImageButton.frame.origin.y + 100, width: 250, height: 50)
        nikolaevImageButton.setTitle("Выпить за любовь 🍻", for: .normal)
        nikolaevImageButton.setTitleColor(.black, for: .normal)
        nikolaevImageButton.layer.borderColor = UIColor.black.cgColor
        nikolaevImageButton.layer.borderWidth = 1
        nikolaevImageButton.center.x = view.center.x
        view.addSubview(nikolaevImageButton)
        
        // назначаем таргеты
        changeImageButton.addTarget(self, action: #selector(changeImage), for: .touchUpInside)
        nikolaevImageButton.addTarget(self, action: #selector(changeFace), for: .touchUpInside)
    }
    // MARK: - CHANGE IMAGE BUTTON
    // сменить изображение
    @objc func changeImage() {
        let picker = UIImagePickerController() // через пикер меняем картинку на другую из галереи
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    // отображаем выбранную в пикере картинку
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }

        dismiss(animated: true)

        for view in self.photoImageView.subviews {
            view.removeFromSuperview()
        }
        photoImageView.image = image
    }
    // MARK: - RECOGNITION BLOCK
    // изменить лица
    @objc func changeFace(_ sender: Any) {
        // распознаем на картинке лица
        let request = VNDetectFaceRectanglesRequest { request, error in
            if let error = error {
                print("Failed to detect:", error)
            }
            // для каждого лица выполняем следующий блок
            request.results?.forEach({ res in
                DispatchQueue.main.async { [weak self] in
                    guard let strongSelf = self else {return}
                    // тут координаты найденного лица
                    guard let faceObs = res as? VNFaceObservation else {return}
                    
                    // определяем координаты и размеры лиц на вью
                    let x =  strongSelf.photoImageView.frame.width * faceObs.boundingBox.origin.x
                    let height = strongSelf.photoImageView.frame.height * faceObs.boundingBox.height
                    let width = strongSelf.photoImageView.frame.width * faceObs.boundingBox.width
                    let y = strongSelf.photoImageView.frame.height * (1 - faceObs.boundingBox.origin.y) - height
                    
                    // инициализируем новый UIImageView
                    let nikolaevImageView = UIImageView(image: UIImage(named: "nikolaev"))
                    nikolaevImageView.frame = CGRect(x: x, y: y, width: width, height: height)
                    nikolaevImageView.contentMode = .scaleAspectFit
                    // отображаем его поверх изображения
                    strongSelf.photoImageView.addSubview(nikolaevImageView)
                }
            })
        }
        
        guard let cgImage = photoImageView.image?.cgImage else {
            return
        }
        DispatchQueue.global().async {
            let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
            
            do {
                try handler.perform([request]) // выполняем блок распознавания и замены лица
            } catch let reqErr {
                print("Failed to perform:", reqErr)
            }
        }
    }
}
